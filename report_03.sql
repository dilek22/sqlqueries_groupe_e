SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 3 : Première réservation effectuée</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_03.htm
SELECT r.reservation_id "Numéro de réservation", r.creation_date "Date création", UPPER(e.last_name) || ' ' || e.first_name "NOM et Prénom de l’employé", UPPER(c.last_name) || ' ' || c.first_name "NOM et Prénom du client"
FROM t_reservation r JOIN t_employee e
ON r.employee_id = e.employee_id JOIN t_customer c
ON c.customer_id = r.buyer_id
WHERE r.creation_date = (SELECT MIN(r.creation_date) 
                         FROM t_reservation r);
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON