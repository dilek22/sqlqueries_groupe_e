SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 12 : Popularité des abonnements</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_12.htm
SELECT p.pass_name "Classement des abonnements"
FROM t_pass p
JOIN t_customer c
ON (p.pass_id = c.pass_id)
GROUP BY p.pass_name
ORDER BY COUNT(*) DESC;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON