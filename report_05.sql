SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 5 : Trajets effectués</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_05.htm
SELECT 'n°' || t.train_id  || ' ' || d.city || TO_CHAR(t.departure_time, ' (DD/MM/YY HH24:MI)') || ' – ' || a.city || TO_CHAR(t.arrival_time, ' (DD/MM/YY HH24:MI)') "Trajet", t.distance || ' km' "Distance parcourue (en km)", t.price || ' €' "Prix initial (en €)"
FROM t_train t JOIN t_station d
ON t.departure_station_id = d.station_id JOIN t_station a
ON t.arrival_station_id = a.station_id 
ORDER BY t.train_id;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON