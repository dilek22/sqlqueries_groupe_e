SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 15 : Organigramme</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_15.htm
SELECT UPPER(e.last_name) || ' ' || e.first_name "NOM et Prénom", COUNT(r.employee_id) "Nombre réservations"
FROM t_employee e
LEFT OUTER JOIN t_reservation r
ON (e.employee_id = r.employee_id)
WHERE manager_id IS NOT NULL
GROUP BY UPPER(e.last_name) || ' ' || e.first_name
ORDER BY COUNT(r.employee_id) DESC;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON