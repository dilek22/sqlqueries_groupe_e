SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 9 : Vitesse moyenne d'un train</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_09.htm
SELECT train_id "Numéro du train", d.city || ' - ' || a.city "Nom du train", ROUND((distance/((arrival_time-departure_time)*24)),0) || ' km/h' "Vitesse moyenne du train"
FROM t_train t 
JOIN t_station d
ON t.departure_station_id = d.station_id 
JOIN t_station a
ON t.arrival_station_id = a.station_id
ORDER BY ROUND((distance/((arrival_time-departure_time)*24)),0) DESC;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON