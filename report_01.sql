SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 1 : Meilleur employé</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_01.htm
SELECT*
FROM (SELECT UPPER(e.last_name) || ’ ‘ || e.first_name “NOM et Prénom”
FROM t_employee e
JOIN t_reservation r
ON r.employee_id = e.employee_id
GROUP BY e.last_name, e.first_name
ORDER BY COUNT(r.reservation_id) DESC)
WHERE ROWNUM <= 1;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON