SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 10 : Réservations Séniors de octobre 2020</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_10.htm
SELECT COUNT(c.customer_id) "Nombre de client avec Réduction Sénior /10/2020"
FROM t_customer c
JOIN t_pass p
ON p.pass_id = c.pass_id
JOIN t_reservation r
ON r.buyer_id = c.customer_id
JOIN t_ticket ti
ON ti.reservation_id = r.reservation_id
JOIN t_wagon_train wagon_tr
ON wagon_tr.wag_tr_id = ti.wag_tr_id
JOIN t_train t
ON t.train_id = wagon_tr.wag_tr_id
WHERE p.pass_name = 'Senior'
AND r.buy_method IS NOT NULL
AND TO_CHAR(t.departure_time, 'MM/YYYY') = '10/2020';
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON