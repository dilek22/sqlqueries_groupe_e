SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 13 : Billets des moins de 25 ans</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_13.htm
SELECT ti.ticket_id "Numéro du billet", UPPER(c.last_name) || ' ' || c.first_name "NOM du Client", t.train_id || ' ' || a.city || TO_CHAR(t.departure_time, ' (DD/MM/YY HH24:MI)') || ' – ' || b.city || TO_CHAR(t.arrival_time, ' (DD/MM/YY HH24:MM)') "Nom du train"
FROM t_train t 
JOIN t_station a 
ON a.station_id = t.departure_station_id 
JOIN t_station b   
ON b.station_id = t.arrival_station_id 
JOIN t_wagon_train w 
ON t.train_id = w.train_id 
JOIN t_wagon wa 
ON w.wagon_id = wa.wagon_id 
JOIN t_ticket ti 
ON w.wag_tr_id = ti.wag_tr_id 
JOIN t_reservation r 
ON ti.reservation_id = r.reservation_id 
JOIN t_customer c 
ON r.buyer_id = c.customer_id 
WHERE MONTHS_BETWEEN(SYSDATE, birth_date) < (25*12) 
AND wa.class_type = 1 
AND departure_time BETWEEN '20/10/2020' AND '26/10/2020' 
AND r.creation_date <= t.departure_time-20 
ORDER BY r.creation_date;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON