SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 2 : Acheteurs réservistes sans billet</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_02.htm
SELECT DISTINCT (UPPER(c.last_name) || ‘ ‘ || c.first_name) "NOM et Prénom de l’employé"
FROM t_reservation r JOIN t_ticket t
ON r.buyer_id != t.customer_id JOIN t_customer c
ON r.buyer_id = c.customer_id
ORDER BY "NOM et Prénom de l’employé";
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON