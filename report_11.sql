SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 11 : Trains de Paris</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_11.htm
SELECT 'n°' || train_id || ' ' || d.city || ' – ' || a.city "Nom du train", pass_name "Titre de l’abonnement", (t_train.price*discount_pct)/100 || ' €' "Tarif Semaine (en €)", (t_train.price*discount_we_pct)/100 || ' €' "Tarif Week-end (en €)"
FROM t_train
CROSS JOIN t_pass JOIN t_station d
ON d.station_id = departure_station_id JOIN t_station a
ON a.station_id = arrival_station_id 
AND d.city = 'Paris'
ORDER BY train_id;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON