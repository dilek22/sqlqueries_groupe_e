SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 16 : Augmentation des coordinateurs</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_16.htm
SELECT employee_id "Numéro de l’employé", UPPER(last_name) ||' '|| first_name "NOM et Prénom", salary+100 || ' €' "Nouveau salaire (en €)"
FROM t_employee
WHERE employee_id IN (SELECT employee_id FROM t_employee WHERE manager_id = 1);
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON