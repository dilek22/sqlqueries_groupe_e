SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 6 : Statistiques</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_06.htm
SELECT emp.nbemp "Nombre d’employés", cus.nbcus "Nombre d’acheteurs", cus.abo "Pourcentage de clients abonnés", res.nbres "Nombre de réservations", tic.nbtic "Nombre de tickets", tr.nbtr "Nombre de trains", sta.nbsta "Nombre de stations"
FROM (SELECT COUNT(e.employee_id) nbemp
FROM t_employee e) emp, 
(SELECT COUNT(c.customer_id) nbcus, TRUNC((COUNT(c.pass_id)/COUNT(c.customer_id)*100), 2) || '%' abo
	FROM t_customer c) cus, 
(SELECT COUNT(r.reservation_id) nbres
	FROM t_reservation r) res, 
(SELECT COUNT(ti.ticket_id ) nbtic
	FROM t_ticket ti ) tic, 
(SELECT COUNT(t.train_id) nbtr
	FROM t_train t) tr, 
(SELECT COUNT(s.station_id) nbsta
	FROM t_station s) sta;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON