SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 4 : Titres d'abonnement</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_04.htm
SELECT UPPER(c.last_name) || ' ' || c.first_name "NOM et Prénom",
CASE  
    WHEN (Months_between('14/10/2020', c.pass_date) > 12) THEN 'Perimé !'
    WHEN (Months_between('14/10/2020', c.pass_date) is NULL) THEN 'Aucun'
ELSE p.pass_name
END "Titre de l'abonnement"
FROM t_customer c
LEFT OUTER JOIN t_pass p
ON (p.pass_id = c.pass_id)
ORDER BY "NOM et Prénom";
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON