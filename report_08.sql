SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 8 : Clients sans abonnement</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_08.htm
SELECT UPPER(last_name) || ' ' || first_name "NOM et Prénom", address "Adresse"
FROM t_customer
WHERE pass_id IS NULL
ORDER BY "NOM et Prénom";
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON