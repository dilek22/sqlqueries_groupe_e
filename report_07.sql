SET PAGESIZE 50
SET ECHO OFF
SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>Rapport 7 : Top-5 des trains</TITLE> -
<link rel='stylesheet' href='style.css' type='text/css'>"
SPOOL report_07.htm
SELECT*
FROM (SELECT d.city || ' - ' || a.city "Top-5 des trains"
FROM t_train t JOIN t_station d
ON t.departure_station_id = d.station_id
JOIN t_station a
ON t.arrival_station_id = a.station_id
JOIN t_wagon_train wt
ON t.train_id = wt.train_id
JOIN t_ticket ti
ON wt.wag_tr_id = ti.wag_tr_id
GROUP BY d.city, a.city, t.train_id
ORDER BY COUNT(ti.ticket_id) DESC)
WHERE ROWNUM < 6;
SPOOL OFF
SET MARKUP HTML OFF
SET ECHO ON